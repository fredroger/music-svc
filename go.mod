module music-svc

go 1.14

require (
	github.com/faiface/beep v1.0.1
	github.com/gin-contrib/cors v1.3.0
	github.com/gin-gonic/contrib v0.0.0-20191209060500-d6e26eeaa607
	github.com/gin-gonic/gin v1.5.0
)
