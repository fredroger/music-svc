import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    nowPlaying: {
       trackName: 'this is the name of the song', progress: 75 
    }
  },
  mutations: {

  },
  actions: {

  },
});
