package main

import (
	"fmt"
	"log"
	"music-svc/api"
	"music-svc/player"
)

// Version represents software version
var Version = "v0.0.0"

// SampleRate is the number of samples per second this player will be set
const SampleRate = 44100

func main() {
	//nowPlaying := api.DefaultNowPlaying()
	server := api.NewServer()

	p := player.NewPlayer(SampleRate)
	playlistEngine, playlistEvents := player.NewPlaylistEngine(p)

	// Listen for playlist events and print them
	go func() {
		for npEvent := range playlistEvents {
			if trackChangedEvent, ok := npEvent.Event.(player.PlaylistTrackChangedEvent); ok {
				fmt.Printf("\r   playing %s --> %f remaining: %s", trackChangedEvent.TrackName, trackChangedEvent.Position, trackChangedEvent.Remaining)
				server.NowPlayingController.UpdateNowPlaying(api.NowPlaying{
					TrackName: trackChangedEvent.TrackName,
					Position:  trackChangedEvent.Position,
					Remaining: trackChangedEvent.Remaining,
					Duration:  trackChangedEvent.Duration.Seconds(),
				})
			} else if trackStarted, ok := npEvent.Event.(player.PlaylistTrackStartedEvent); ok {
				log.Println("Track started:", trackStarted.TrackName)
			}
		}
	}()

	// Start the playlist audio engine
	go playlistEngine.Run()

	// Start the web API
	server.Serve()
}
