package api

import (
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/contrib/static"
	"github.com/gin-gonic/gin"
)

// Server serves the front end and the APIs of the player
type Server struct {
	NowPlayingController *NowPlayingController
}

// NewServer creates a new Server
func NewServer() *Server {
	server := Server{}
	server.NowPlayingController = NewNowPlayingController()

	return &server
}

// Serve declares both the front end route and the APIs route and starts to listen for http request
func (s *Server) Serve() {

	r := gin.Default()

	// Serve the front end
	r.Use(cors.Default(), static.Serve("/", static.LocalFile("./player-console/dist", true)))

	// Setup route group for the API
	api := r.Group("/api")
	{
		api.GET("/nowPlaying", s.NowPlayingController.handleGetNowPlaying)
		api.GET("/nowPlaying/Stream", s.NowPlayingController.handleStreamNowPlaying)
	}

	// Start web server
	r.Run(":1666")
}
