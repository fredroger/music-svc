package api

import (
	"io"
	"net/http"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
)

//NowPlaying represents the now playing state
type NowPlaying struct {
	Duration  float64       `json:"duration"`
	Position  float64       `json:"position"`
	Remaining time.Duration `json:"remaining"`
	TrackName string        `json:"trackname"`
}

// DefaultNowPlaying returns a NowPlaying with default values
func DefaultNowPlaying() NowPlaying {
	n := NowPlaying{}
	n.Reset()

	return n
}

// Reset puts position to 0 and track name to nothing is playing
func (n *NowPlaying) Reset() {
	n.Position = 0
	n.Duration = 0
	n.Remaining = 0
	n.TrackName = "nothing is playing"
}

// NowPlayingController is reponsible for now playing routes
type NowPlayingController struct {
	nowPlaying          NowPlaying
	nowPlayingListeners []chan NowPlaying
	mu                  sync.Mutex
}

// NewNowPlayingController initialize a nowPlayingController with default values
func NewNowPlayingController() *NowPlayingController {
	nowPlayingController := NowPlayingController{nowPlaying: NowPlaying{TrackName: "Nothing is playing"}}

	return &nowPlayingController
}

// UpdateNowPlaying will update now playing info and trigger the now playing stream
func (n *NowPlayingController) UpdateNowPlaying(nowPlaying NowPlaying) {
	n.mu.Lock()
	n.nowPlaying = nowPlaying
	n.mu.Unlock()

	n.notify(n.nowPlaying)
}

func (n *NowPlayingController) handleGetNowPlaying(c *gin.Context) {
	c.String(http.StatusNotImplemented, "GET nowplaying is not implemented yet")
}

func (n *NowPlayingController) handleStreamNowPlaying(c *gin.Context) {

	nowPlayingListener := make(chan NowPlaying)
	n.addListener(nowPlayingListener)

	isClientDisconnected := c.Stream(func(w io.Writer) bool {
		if event, ok := <-nowPlayingListener; ok {
			c.SSEvent("message", event)
			return true
		}

		n.removeListener(nowPlayingListener)
		return false
	})

	if isClientDisconnected {
		n.removeListener(nowPlayingListener)
	}
}

func (n *NowPlayingController) addListener(c chan NowPlaying) {
	n.mu.Lock()
	defer n.mu.Unlock()
	n.nowPlayingListeners = append(n.nowPlayingListeners, c)
}

func (n *NowPlayingController) removeListener(c chan NowPlaying) {
	n.mu.Lock()
	defer n.mu.Unlock()
	for i, v := range n.nowPlayingListeners {
		if v == c {
			n.nowPlayingListeners = append(n.nowPlayingListeners[:i], n.nowPlayingListeners[i+1:]...)
			return
		}
	}
}

func (n *NowPlayingController) notify(event NowPlaying) {
	// O(n), won't scale but it's good enough for now
	for _, v := range n.nowPlayingListeners {
		v <- event
	}
}
