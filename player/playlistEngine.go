package player

import (
	"log"
	"path/filepath"
	"time"
)

// some audio samples
const (
	AudioSampleFile1      = "audio-samples/bensound-acousticbreeze.mp3"
	AudioSampleFile2      = "audio-samples/bensound-creativeminds.mp3"
	AudioSampleFile3      = "audio-samples/bensound-moose.mp3"
	ShortAudioSampleFile1 = "audio-samples/short1.mp3"
)

// PlaylistEngine is responsible to play the entire playlist, in order.
// When a track is invalid, it skips to the next one.
type PlaylistEngine struct {
	player                 *Player
	playlist               []string
	playlistNextTrackIndex int
	skipTrackInPlaylist    bool
	nowPlaying             NowPlaying
	playlistEvents         chan PlaylistEvent
}

// PlaylistEvent ...
type PlaylistEvent struct {
	Event interface{}
}

// PlaylistTrackChangedEvent ...
type PlaylistTrackChangedEvent struct {
	TrackName     string
	PlaylistIndex int
	Position      float64
	Duration      time.Duration
	Remaining     time.Duration
}

// PlaylistTrackStartedEvent ...
type PlaylistTrackStartedEvent struct {
	TrackName     string
	PlaylistIndex int
	Duration      time.Duration
}

// NewPlaylistEngine creates a basic playlist with 3 short tracks
func NewPlaylistEngine(p *Player) (*PlaylistEngine, <-chan PlaylistEvent) {
	events := make(chan PlaylistEvent, 10)
	return &PlaylistEngine{
		player:         p,
		playlist:       []string{ShortAudioSampleFile1, ShortAudioSampleFile1, ShortAudioSampleFile1},
		playlistEvents: events,
	}, events
}

// Run starts the playlist engine loop. It should be call through a goroutine
func (e *PlaylistEngine) Run() {
	// Start the first track
	e.playNext()

	for {
		select {
		case <-e.player.Done:
			e.playNext()
		case <-time.After(1 * time.Second):
			if e.skipTrackInPlaylist {
				e.skipTrackInPlaylist = false
				log.Println("skipping track")
				e.playNext()
			}
			if e.playlistNextTrackIndex == 0 {
				// Here we have played all tracks of the current playlist.
				// In the future, we could decide what to do next but for now, let's play it again!
				log.Println("returning at the beginning of the playlist")
				e.playNext()
			} else {
				e.playlistEvents <- PlaylistEvent{Event: PlaylistTrackChangedEvent{
					PlaylistIndex: e.playlistNextTrackIndex - 1,
					TrackName:     filepath.Base(e.playlist[e.playlistNextTrackIndex-1]),
					Duration:      e.player.NowPlaying.Duration,
					Position:      e.player.Position().Seconds(),
					Remaining:     e.player.Remaining(),
				}}
			}
		}
	}
}

func (e *PlaylistEngine) playNext() {
	if e.playlistNextTrackIndex < len(e.playlist) {
		trackErr := e.player.PlayTrackFromFile(e.playlist[e.playlistNextTrackIndex])
		if trackErr != nil {
			log.Println("[TrackError]", trackErr)
			e.skipTrackInPlaylist = true
		} else {
			e.playlistEvents <- PlaylistEvent{Event: PlaylistTrackStartedEvent{
				PlaylistIndex: e.playlistNextTrackIndex,
				TrackName:     filepath.Base(e.playlist[e.playlistNextTrackIndex]),
				Duration:      e.player.NowPlaying.Duration,
			}}
		}
		e.playlistNextTrackIndex++
	} else {
		//nowPlaying.Reset()
		log.Println("no more song in the playlist")
		e.playlistNextTrackIndex = 0
	}
}
