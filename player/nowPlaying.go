package player

import (
	"time"

	"github.com/faiface/beep"
)

// NowPlaying holds info about the actual decoded song
type NowPlaying struct {
	Duration      time.Duration
	trackFilePath string
	format        beep.Format
	streamer      beep.StreamSeekCloser
}

// IsTrackValid returns true if the track is loaded and valid
func (np *NowPlaying) IsTrackValid() bool {
	return (np.streamer != nil)
}
