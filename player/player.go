package player

import (
	"log"
	"os"
	"time"

	"github.com/faiface/beep"
	"github.com/faiface/beep/effects"
	"github.com/faiface/beep/mp3"
	"github.com/faiface/beep/speaker"
)

// Player represents an audio player
type Player struct {
	Done       chan bool
	NowPlaying NowPlaying
	sampleRate beep.SampleRate
	ctrl       *beep.Ctrl
	volumeCtrl *effects.Volume
}

// NewPlayer initialize an audio player with a sample rate
func NewPlayer(sampleRate int) *Player {

	var p Player
	p.Done = make(chan bool)

	p.sampleRate = beep.SampleRate(sampleRate)
	speaker.Init(p.sampleRate, p.sampleRate.N(time.Second/10))

	return &p
}

// PlayTrackFromFile plays audio from a mp3 file path
func (p *Player) PlayTrackFromFile(trackFilePath string) error {

	// Open the file on the disk.
	f, err := os.Open(trackFilePath)
	if err != nil {
		return err
	}

	// Decode it.
	streamer, format, err := mp3.Decode(f)
	if err != nil {
		return err
	}

	// Update actual track info.
	p.NowPlaying.trackFilePath = trackFilePath
	p.NowPlaying.format = format
	p.NowPlaying.streamer = streamer
	p.NowPlaying.Duration = format.SampleRate.D(streamer.Len()).Round(time.Second)

	// The speaker's sample rate is fixed at 44100. Therefore, we need to
	// resample the file in case it's in a different sample rate.
	resampled := beep.Resample(4, format.SampleRate, p.sampleRate, streamer)

	left := multiplyChannels(1, 0, resampled)
	//rightCh = effects.Mono(multiplyChannels(0, 1, rightCh))

	// Add a controller for pause
	p.ctrl = &beep.Ctrl{Streamer: left, Paused: false}

	// Add a controller for volume
	p.volumeCtrl = &effects.Volume{
		Streamer: p.ctrl,
		Base:     2,
		Volume:   0,
		Silent:   false,
	}

	// Play a sequence of streams. First one is the track, second one is empty and is used to annonce the track is finished.
	speaker.Play(beep.Seq(p.volumeCtrl, beep.Callback(func() {
		p.Done <- true
	})))

	return nil
}

// Stop stops playing the current audio track
func (p *Player) Stop() {
	speaker.Lock()
	p.ctrl.Streamer = nil
	speaker.Unlock()
}

// Pause makes the current audio track paused
func (p *Player) Pause() {
	speaker.Lock()
	p.ctrl.Paused = !p.ctrl.Paused
	speaker.Unlock()
}

// Mute puts the audio output to muted
func (p *Player) Mute() {
	speaker.Lock()
	p.volumeCtrl.Silent = !p.volumeCtrl.Silent
	speaker.Unlock()
}

// IncreaseVolume will add 0.5 on the actual volume (exponential)
func (p *Player) IncreaseVolume() {
	p.changeVolume(0.5)
}

// DecreaseVolume will remove 0.5 on the actual volume (exponential)
func (p *Player) DecreaseVolume() {
	p.changeVolume(-0.5)
}

// Position gives the current position (Duration) of the audio stream
func (p *Player) Position() (duration time.Duration) {
	speaker.Lock()
	if p.NowPlaying.IsTrackValid() {
		duration = p.NowPlaying.format.SampleRate.D(p.NowPlaying.streamer.Position()).Round(time.Second)
	}
	speaker.Unlock()

	return duration
}

// Remaining gives the current remaining "Duration" of the audio stream
func (p *Player) Remaining() (remaining time.Duration) {
	speaker.Lock()
	if p.NowPlaying.IsTrackValid() {
		remaining = p.NowPlaying.format.SampleRate.D(p.NowPlaying.streamer.Len() - p.NowPlaying.streamer.Position()).Round(time.Second)
	}
	speaker.Unlock()

	return remaining
}

func (p *Player) changeVolume(diff float64) {
	speaker.Lock()
	p.volumeCtrl.Volume += diff
	log.Println("Volume is now:", p.volumeCtrl.Volume)
	speaker.Unlock()
}

func multiplyChannels(left, right float64, s beep.Streamer) beep.Streamer {
	return beep.StreamerFunc(func(samples [][2]float64) (n int, ok bool) {
		n, ok = s.Stream(samples)
		for i := range samples[:n] {
			samples[i][0] *= left
			samples[i][1] *= right
		}
		return n, ok
	})
}
